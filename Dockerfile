FROM 1dev/server

RUN apt-get install wget -y

RUN wget https://download-gcdn.ej-technologies.com/jprofiler/jprofiler_linux_12_0_1.tar.gz -P /tmp/ &&\
 tar -xzf /tmp/jprofiler_linux_12_0_1.tar.gz -C /usr/local &&\
 rm /tmp/jprofiler_linux_12_0_1.tar.gz

ENV JPAGENT_PATH="-agentpath:/usr/local/jprofiler8/bin/linux-x64/libjprofilerti.so=nowait"

CMD ["/root/bin/entrypoint.sh"]